<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsClientes extends Model
{
    protected $fillable = array(
        'nome',
        'sigla',
        'path_logo',
        'status'
    );

    public function fabricas()
    {
        return $this->belongsToMany('App\OsFabricas', 'os_clientes_fabricas', 'id_cliente', 'id_fabrica');
    }

    public function parametros()
    {
        return $this->belongsToMany('App\OsParametros', 'os_clientes_parametros', 'id_cliente', 'id_parametro');
    }

    public function servicos()
    {
    	return $this->hasMany('App\OsServicos', 'id_cliente');
    }
}
