<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsSaidas extends Model
{
    protected $fillable = array(
        'nome'
    );
}
