<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competencias extends Model
{
    protected $fillable = array(
		'descricao',
        'ordem'
	);

    public function itensCompetencias()
    {
    	return $this->hasMany('App\ItensCompetencias', 'id_competencia');
    }    
}
