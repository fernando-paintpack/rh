<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{
    public function itensCompetencias()
    {
        return $this->belongsToMany('App\ItensCompetencias', 'avaliacoes_itens_competencias_notas', 'id_nota', 'id_item_competencia');
    }

    public function avaliacoes()
    {
        return $this->belongsToMany('App\Avaliacoes', 'avaliacoes_itens_competencias_notas', 'id_nota', 'id_avaliacao');
    }
}
