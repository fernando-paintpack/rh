<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsServicos extends Model
{
    protected $fillable = array(
        'id_cliente',
        'rotulo',
        'dt_entrada',
        'codigo',
        'codigo_exal',
        'job',
        'job_anterior',
        'tipo_saida',
        'path_xml',
        'observacoes'
    );

    public function clientes()
    {
    	return $this->belongsTo('App\OsClientes', 'id_cliente');
    }

    public function cores()
    {
    	return $this->hasMany('App\OsCores', 'id_servico');
    }
}
