<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user()->id_perfil);
        if (!in_array(Auth::user()->id_perfil, [1, 2, 4])) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                switch (Auth::user()->id_perfil) {
                    case 3:
                        return redirect('avaliacoes');
                        break;
                    case 5:
                        return redirect('os/servicos');
                        break;
                    default:
                        return view('/login');
                        break;
                }
            }
        }

        return $next($request);
    }
}
