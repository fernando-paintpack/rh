<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\ServicosController;
use App\OsServicos;
use App\OsClientes;
use App\OsFabricas;
use App\OsCores;
use App\OsSaidas;
use App\OsVolumes;
use App\OsParametros;
use App\Colaboradores;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class OsServicoController extends Controller
{
    private $servico;

    public function __construct(OsServicos $servico)
    {
    	$this->servico = $servico;
    }

    public function index()
    {
        $servicos = [];
        $clientes = [];
    	$allServicos = OsServicos::orderBy('id_cliente')->orderBy('job')->get();
        $allClientes = OsClientes::all();
        foreach ($allServicos as $servico) {
            $data = date_create($servico->dt_entrada);
            $servico->dt_entrada = date_format($data, 'd/m/Y');
            $servicos[] = $servico;
        }
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        return view('os-servicos.index', compact('servicos', 'clientes'));
    }

    public function search($os)
    {
        $servicos = [];
        $clientes = [];
        $allServicos = $this->servico->where('rotulo', 'like', '%'.$os.'%')->orWhere('codigo', 'like', '%'.$os.'%')->orderBy('job')->get();
        $allClientes = OsClientes::all();
        foreach ($allServicos as $servico) {
            $data = date_create($servico->dt_entrada);
            $servico->dt_entrada = date_format($data, 'd/m/Y');
            $servicos[] = $servico;
        }
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        return view('os-servicos.index', compact('servicos', 'clientes'));
    }

    public function getDestinatarios()
    {
    	$colaboradores = Colaboradores::where('email', '<>', null)->get();
        return view('os-servicos.destinatarios', compact('colaboradores'));
    }

    public function postDestinatarios(Request $request)
    {
        $colaboradores = new Colaboradores;
        $allColaboradores = $colaboradores::all();
        foreach ($allColaboradores as $value) {
            $colaborador = $colaboradores->find($value->id);
            $colaborador->update(['dest_email_os' => 0]);
        }
        if(isset($request->destinatarios)){
            foreach ($request->destinatarios as $destinatario) {
                $colaborador = $colaboradores->find($destinatario);
                $colaborador->update(['dest_email_os' => 1]);
            }
        }
    	return redirect()->route("os.servicos.get.destinatarios");
    }

    public function getNova()
    {
        $allClientes = OsClientes::all();
        $allFabricas = OsFabricas::all();
        $allSaidas = OsSaidas::all();
        $allVolumes = OsVolumes::all();
        $clientes = [];
        $fabricas = [];
        $saidas = [];
        $volumes = [];
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        foreach ($allFabricas as $fabrica) {
            $fabricas[$fabrica->id] = $fabrica->nome;
        }
        foreach ($allSaidas as $saida) {
            $saidas[$saida->nome] = $saida->nome;
        }
        foreach ($allVolumes as $volume) {
            $volumes[$volume->nome] = $volume->nome;
        }
        return view('os-servicos.nova', compact('clientes', 'fabricas', 'saidas', 'volumes'));
    }

    public function getNovaCopia($id)
    {
        $servico = $this->servico->find($id);
        $cliente = OsClientes::find($servico->id_cliente);
        $allClientes = OsClientes::all();
        $allFabricas = OsFabricas::all();
        $allSaidas = OsSaidas::all();
        $allVolumes = OsVolumes::all();
        $clientes = [];
        $fabricas = [];
        $saidas = [];
        $volumes = [];
        $xml = null;

        foreach ($allClientes as $value) {
            $clientes[$value->id] = $value->nome;
        }
        foreach ($allFabricas as $fabrica) {
            $fabricas[$fabrica->id] = array('nome' => $fabrica->nome, 'sigla' => $fabrica->sigla);
        }
        foreach ($allSaidas as $saida) {
            $saidas[$saida->nome] = $saida->nome;
        }
        foreach ($allVolumes as $volume) {
            $volumes[strtolower($volume->nome)] = $volume->nome;
        }

        foreach ($cliente->fabricas as $cliFab) {
            $fabs[] = $cliFab->id;
        }
        $servico->clienteFabricas = $fabs;
        $servico->parametros = $cliente->parametros;

        $job = explode('-', $servico->job);
        $servico->job_num = $job[0] . '-' . $job[1];
        $servico->job_cli = $job[2];
        $servico->job_rev = substr($job[3], 1);

        $date = str_replace('-', '/', $servico->dt_entrada);
        $entrada = date_create($date);
        $servico->dt_entrada = date_format($entrada, 'd/m/Y');

        if(File::isFile(storage_path().$servico->path_xml))
            $xml = simplexml_load_file(storage_path().$servico->path_xml);

        $servico->fabricas = explode('-', $xml->FABRICAS);

        $xml = get_object_vars($xml);

        return view('os-servicos.nova-copia', compact('servico', 'xml', 'clientes', 'cliente', 'fabricas', 'saidas', 'volumes'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->id_cliente) || empty($request->tipo_saida) || empty($request->rotulo) || empty($request->job_num) || empty($request->job_cli)
        || empty($request->job_rev) || empty($request->dt_entrada) || empty($request->codigo) || empty($request->fabricas)){
            return false;
        }

        header('Content-type: application/json');

        $count = 1;
        $cores = [];
        $novasCores = new OsCores;
        $dados = $request->all();
        $dados['job'] = $dados['job_num'] . '-' . $dados['job_cli'] . '-R' . $dados['job_rev'];

        if (count($this->servico->where('job', $dados['job'])->get()) > 0) {
            echo 'O job informado já está cadastrado no sistema.';
            die;
        }

        $allColaboradores = Colaboradores::where('dest_email_os', 1)->get();
        foreach ($allColaboradores as $colaborador) {
            $total[] = $colaborador->email;
        }

        $cliente = OsClientes::find($dados['id_cliente']);

        $date = str_replace('/', '-', $dados['dt_entrada']);
        $entrada = date_create($date);
        $dados['dt_entrada'] = date_format($entrada, 'Y-m-d');

        $dados['fabricas'] = implode('-', $dados['fabricas']);

        foreach ($dados['cores'] as $cor) {
            if(!empty($cor)){
                $cores[] = $cor;
                $dados['parametros']['COR' . $count] = $cor;
                $count++;
            }
        }
        $dados['cores'] = $cores;

        $dados['parametros']['CLIENTE'] = $cliente->nome;
        $dados['parametros']['JOBNAME'] = $dados['rotulo'];
        $dados['parametros']['JOB'] = $dados['job'];
        $dados['parametros']['DATA_OS'] = $request->dt_entrada;
        if(isset($dados['codigo_exal']) && $dados['codigo_exal'] != ''){
            $dados['parametros']['CODEXAL'] = $dados['codigo_exal'];
            $dados['parametros']['CODCLIENTE'] = $dados['codigo'];
        }
        else{
            $dados['parametros']['CODDESENVOLVIMENTO'] = $dados['codigo'];
        }
        $dados['parametros']['JOBANTERIOR'] = $dados['job_anterior'];
        $dados['parametros']['FABRICAS'] = $dados['fabricas'];
        $dados['parametros']['SAIDA'] = $dados['tipo_saida'];
        $dados['parametros']['VOLUME'] = $dados['volume'];
        $dados['parametros']['CONTATO1'] = $dados['contato_um'];
        $dados['parametros']['CONTATO2'] = $dados['contato_dois'];

        foreach ($dados['parametros'] as $key => $parametro) {
            $parametros[$key] = strtoupper($parametro);
        }

        $service = new ServicosController;

        $fileName = $dados['job'] . '.xml';
        $xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><JobParameters></JobParameters>");
        $service->arrayToXml($parametros, $xml);
        $xml_file = $xml->asXML(storage_path() . '/files/uploads/' . $fileName);

        if($xml){
            $dados['path_xml'] = '/files/uploads/' . $fileName;
        }else{
            echo 'Erro na criação de arquivo XML.';
            die;
        }

        $servico = $this->servico;
        $os = $servico->create($dados);

        foreach ($dados['cores'] as $value) {
             $novasCores->create(['id_servico' => $os->id, 'nome' => $value]);
        }

        return redirect()->route("os.servicos.index");
    }

    public function getEdit($id)
    {
        $servico = $this->servico->find($id);
        $fabricas = [];
        $xml = null;

        $allSaidas = OsSaidas::all();
        $allVolumes = OsVolumes::all();
        $saidas = [];
        $volumes = [];
        foreach ($allSaidas as $saida) {
            $saidas[$saida->nome] = $saida->nome;
        }
        foreach ($allVolumes as $volume) {
            $volumes[strtoupper($volume->nome)] = $volume->nome;
        }

        $cliente = OsClientes::find($servico->id_cliente);
        $servico->clienteFabricas = $cliente->fabricas;
        $servico->parametros = $cliente->parametros;

        $job = explode('-', $servico->job);
        $servico->job_num = $job[0] . '-' . $job[1];
        $servico->job_cli = $job[2];
        $servico->job_rev = substr($job[3], 1);

        $date = str_replace('-', '/', $servico->dt_entrada);
        $entrada = date_create($date);
        $servico->dt_entrada = date_format($entrada, 'd/m/Y');

        if(File::isFile(storage_path().$servico->path_xml))
            $xml = simplexml_load_file(storage_path().$servico->path_xml);

        $servico->fabricas = explode('-', $xml->FABRICAS);

        foreach ($servico->clienteFabricas as $fabrica) {
            $fabricas[$fabrica->id] = array('nome' => $fabrica->nome, 'sigla' => $fabrica->sigla);
        }

        $xml = get_object_vars($xml);

        return view('os-servicos.edit', compact('servico', 'xml', 'cliente', 'fabricas', 'saidas', 'volumes'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->tipo_saida) || empty($request->rotulo) || empty($request->job_num) || empty($request->job_cli)
        || empty($request->job_rev) || empty($request->dt_entrada) || empty($request->codigo) || empty($request->fabricas)){
            return false;
        }

        $count = 1;
        $cores = [];
        $novasCores = new OsCores;
        $dados = $request->all();
        $dados['job'] = $dados['job_num'] . '-' . $dados['job_cli'] . '-R' . $dados['job_rev'];

        $servico = $this->servico->find($id);

        $cliente = OsClientes::find($dados['id_cliente']);

        $date = str_replace('/', '-', $dados['dt_entrada']);
        $entrada = date_create($date);
        $dados['dt_entrada'] = date_format($entrada, 'Y-m-d');

        $dados['fabricas'] = implode('-', $dados['fabricas']);

        foreach ($dados['cores'] as $cor) {
            if(!empty($cor)){
                $cores[] = $cor;
                $dados['parametros']['COR' . $count] = $cor;
                $count++;
            }
        }
        $dados['cores'] = $cores;

        $dados['parametros']['CARTELA'] = isset($dados['parametros']['CARTELA']) ? $dados['parametros']['CARTELA'] : "0";
        $dados['parametros']['WETONWET'] = isset($dados['parametros']['WETONWET']) ? $dados['parametros']['WETONWET'] : "0";

        $dados['parametros']['CLIENTE'] = $cliente->nome;
        $dados['parametros']['JOBNAME'] = $dados['rotulo'];
        $dados['parametros']['JOB'] = $dados['job'];
        $dados['parametros']['DATA_OS'] = $request->dt_entrada;
        if(isset($dados['codigo_exal']) && $dados['codigo_exal'] != ''){
            $dados['parametros']['CODEXAL'] = $dados['codigo_exal'];
            $dados['parametros']['CODCLIENTE'] = $dados['codigo'];
        }
        else{
            $dados['parametros']['CODDESENVOLVIMENTO'] = $dados['codigo'];
        }
        $dados['parametros']['JOBANTERIOR'] = $dados['job_anterior'];
        $dados['parametros']['FABRICAS'] = $dados['fabricas'];
        $dados['parametros']['SAIDA'] = $dados['tipo_saida'];
        $dados['parametros']['VOLUME'] = $dados['volume'];

        foreach ($dados['parametros'] as $key => $parametro) {
            $parametros[$key] = strtoupper($parametro);
        }

        $service = new ServicosController;

        if(File::isFile(storage_path().$servico->path_xml)){
            File::delete(storage_path().$servico->path_xml);
        }

        $fileName = $dados['job'] . '.xml';
        $xml = new \SimpleXMLElement("<?xml version=\"1.0\"?><JobParameters></JobParameters>");
        $service->arrayToXml($parametros, $xml);
        $xml_file = $xml->asXML(storage_path() . '/files/uploads/' . $fileName);

        if($xml){
            $dados['path_xml'] = '/files/uploads/' . $fileName;
        }else{
            echo 'Erro na criação de arquivo XML.';
            die;
        }

        foreach ($servico->cores as $cor) {
            $removeCor = OsCores::find($cor->id);
            $removeCor->delete();
        }

        $servico->update($dados);

        foreach ($dados['cores'] as $value) {
             $novasCores->create(['id_servico' => $servico->id, 'nome' => $value]);
        }

        return redirect()->route("os.servicos.index");
    }

    public function sendEmail($id)
    {
        $xml = null;
        $count = 1;
        $dados = [];
        $total = [];
        $servico = $this->servico->find($id);

        if(File::isFile(storage_path().$servico->path_xml))
            $xml = simplexml_load_file(storage_path().$servico->path_xml);

        $xml = get_object_vars($xml);

        foreach ($servico->cores as $cor) {
            $cores[] = $cor->nome;
        }

        $dados['cores'] = $cores;

        $allColaboradores = Colaboradores::where('dest_email_os', 1)->get();

        foreach ($allColaboradores as $colaborador) {
            $total[] = $colaborador->email;
        }


        $dados['parametros']['CLIENTE'] = $xml['CLIENTE'];
        $dados['parametros']['JOBNAME'] = $xml['JOBNAME'];
        $dados['parametros']['JOB'] = $xml['JOB'];
        $dados['parametros']['DATA_OS'] = $xml['DATA_OS'];
        if(isset($xml['CODEXAL']) && $xml['CODEXAL'] != ''){
            $dados['parametros']['CODEXAL'] = $xml['CODEXAL'];
            $dados['parametros']['CODCLIENTE'] = $xml['CODCLIENTE'];
        }
        else{
            $dados['parametros']['CODDESENVOLVIMENTO'] = $xml['CODDESENVOLVIMENTO'];
        }

        $data = array(
            'colaboradores' => $total,
            'parametros' => $dados['parametros'],
            'cores' => $dados['cores'],
            'observacoes' => $servico->observacoes,
            'job' => $xml['CLIENTE'] . ' - ' . $xml['JOB'] . ' - ' . $xml['JOBNAME'],
            'fileName' => $servico->path_xml
        );

        Mail::send('os-servicos.email', $data, function ($message) use($data) {
            $message->from('esko@paintpack.com.br', 'Email OS');
            $message->to($data['colaboradores'])->subject('OS - ' . $data['job']);
            $message->attach(storage_path() . $data['fileName'], array(
                'mime' => 'application/xml'
            ));
        });

        return redirect()->route("os.servicos.index");
    }

    public function getImprimir($id)
    {
        $xml = null;
        $parametros = [];
        $servico = $this->servico->find($id);
        $param = new OsParametros;
        $cliente = OsClientes::find($servico->id_cliente);

        $data = date_create($servico->dt_entrada);
        $servico->dt_entrada = date_format($data, 'd/m/Y');

        if(File::isFile(storage_path().$servico->path_xml))
            $xml = simplexml_load_file(storage_path().$servico->path_xml);
        $xml = get_object_vars($xml);

        $parametros = array(
            'CARTELA' => isset($xml['CARTELA']) ? $xml['CARTELA'] : '',
            'BARCODE' => isset($xml['BARCODE']) ? $xml['BARCODE'] : '',
            'VOLUME' => isset($xml['VOLUME']) ? $xml['VOLUME'] : '',
            'SAIDA' => isset($xml['SAIDA']) ? $xml['SAIDA'] : '',
            'COR-BARRAS' => isset($xml['COR-BARRAS']) ? $xml['COR-BARRAS'] : '',
            'COR-BARRAS-NUMEROS' => isset($xml['COR-BARRAS-NUMEROS']) ? $xml['COR-BARRAS-NUMEROS'] : '',
            'COR-TEXTO-LEGAL' => isset($xml['COR-TEXTO-LEGAL']) ? $xml['COR-TEXTO-LEGAL'] : '',
            'COR-TAB-NUTRICIONAL' => isset($xml['COR-TAB-NUTRICIONAL']) ? $xml['COR-TAB-NUTRICIONAL'] : '',
            'TEXTO-EDITAVEL' => isset($xml['TEXTO-EDITAVEL']) ? $xml['TEXTO-EDITAVEL'] : '',
            'TIPO-TEXTO' => isset($xml['TIPO-TEXTO']) ? $xml['TIPO-TEXTO'] : '',
            'TEVE-AMOSTRA' => isset($xml['TEVE-AMOSTRA']) ? $xml['TEVE-AMOSTRA'] : '',
            'COD-AMOSTRA' => isset($xml['COD-AMOSTRA']) ? $xml['COD-AMOSTRA'] : '',
            'VERSAO-AMOSTRA' => isset($xml['VERSAO-AMOSTRA']) ? $xml['VERSAO-AMOSTRA'] : '',
            'CONTATO1' => isset($xml['CONTATO1']) ? $xml['CONTATO1'] : '',
            'CONTATO2' => isset($xml['CONTATO2']) ? $xml['CONTATO2'] : '',
            'PLACAS-DIGITAIS' => isset($xml['NumPlacasDig']) ? $xml['NumPlacasDig'] : ''
        );

        foreach ($servico->cores as $cor) {
            $cores[] = $cor->nome;
        }

        $allFabricas = OsFabricas::whereIn('sigla', explode('-', $xml['FABRICAS']))->get();
        foreach ($allFabricas as $fab) {
            $fabs[] = $fab->nome;
        }
        $fabricas = implode(' - ', $fabs);

        return view('os-servicos.imprimir', compact('servico', 'parametros', 'cliente', 'cores', 'fabricas'));
    }

    public function getRemove($id)
    {
        $cores = new OsCores;
        $servico = $this->servico->find($id);
        foreach ($servico->cores as $cor) {
            $removeCor = $cores->find($cor->id);
            $removeCor->delete();
        }

        if(File::isFile(storage_path().$servico->path_xml)){
            File::delete(storage_path().$servico->path_xml);
        }

        $servico->delete();

        return redirect()->route("os.servicos.index");
    }
}
