<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OsFabricas;
use App\OsClientes;

use App\Http\Requests;

class OsFabricaController extends Controller
{
    private $fabrica;

    public function __construct(OsFabricas $fabrica)
    {
    	$this->fabrica = $fabrica;
    }

    public function index()
    {
    	$fabricas = OsFabricas::all();
    	return view('os-fabricas.index', compact('fabricas'));
    }

    public function getNova()
    {
        $allClientes = OsClientes::all();
        $clientes = [];
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        return view('os-fabricas.nova', compact('clientes'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome) && empty($request->sigla)){
            return false;
        }

        $fabrica = $this->fabrica;

        if(!is_null($request->input('clientes')))
            $fabrica->create($request->all())->clientes()->sync($request->input('clientes'));
        else
            $fabrica->create($request->all());

        return redirect()->route("os.fabricas.index");
    }

    public function getEdit($id)
    {
        $clientes = [];
        $clientesIds = [];
        $editFabrica = $this->fabrica->find($id);
        $allClientes = OsClientes::all();
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        foreach ($editFabrica->clientes as $value) {
            $clientesIds[] = $value->id;
        }
        return view('os-fabricas.edit', compact('editFabrica', 'clientes', 'clientesIds'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome) && empty($request->sigla)){
            return false;
        }

        $fabrica = $this->fabrica->find($id);

        if(isset($request->clientes))
            $fabrica->clientes()->sync($request->input('clientes'));
        else
            $fabrica->clientes()->detach();

        $fabrica->update($request->all());

        return redirect()->route("os.fabricas.index");
    }

    public function getRemove($id)
    {
        $fabrica = $this->fabrica->find($id);
        $fabrica->clientes()->detach();
        $fabrica->delete();

        return redirect()->route("os.fabricas.index");
    }
}
