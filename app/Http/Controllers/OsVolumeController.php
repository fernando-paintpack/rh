<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OsVolumes;

use App\Http\Requests;

class OsVolumeController extends Controller
{
    private $volume;

    public function __construct(OsVolumes $volume)
    {
    	$this->volume = $volume;
    }

    public function index()
    {
    	$volumes = OsVolumes::all();
    	return view('os-volumes.index', compact('volumes'));
    }

    public function getNova()
    {
        return view('os-volumes.nova');
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $volume = $this->volume;
        $volume->create($request->all());

        return redirect()->route("os.volumes.index");
    }

    public function getEdit($id)
    {
        $editVolume = $this->volume->find($id);
        return view('os-volumes.edit', compact('editVolume'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $volume = $this->volume->find($id);
        $volume->update($request->all());

        return redirect()->route("os.volumes.index");
    }

    public function getRemove($id)
    {
        $volume = $this->volume->find($id);
        $volume->delete();

        return redirect()->route("os.volumes.index");
    }
}
