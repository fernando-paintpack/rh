<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colaboradores;

use App\Http\Requests;

class ColaboradorController extends Controller
{
    private $colaborador;

    public function __construct(Colaboradores $colaborador)
    {
    	$this->colaborador = $colaborador;
    }

    public function index()
    {
        $colaboradores = Colaboradores::all();
        return view('colaboradores.index', compact('colaboradores'));
    }

    public function getNova()
    {
        $avals = Colaboradores::where('is_aval', 1)->get();
        $avaliadores = array();
        foreach ($avals as $item) {
            $avaliadores[$item->nome . ' ' . $item->sobrenome] = $item->nome . ' ' . $item->sobrenome;
        }
        return view('colaboradores.nova', compact('avaliadores'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $dados = $request->all();
        $dados['is_aval'] = isset($dados['is_aval']) ? 1 : 0;

        $colaborador = $this->colaborador;
        $colaborador->create($dados);
        return redirect()->route("colaboradores.index");
    }

    public function getEdit($id)
    {
        $editColaborador = $this->colaborador->find($id);
        $avals = Colaboradores::where('is_aval', 1)->where('id', '<>', $id)->get();
        $avaliadores = array();
        foreach ($avals as $aval) {
            $avaliadores[$aval->nome . ' ' . $aval->sobrenome] = $aval->nome . ' ' . $aval->sobrenome;
        }
        return view('colaboradores.edit', compact('editColaborador', 'avaliadores'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $dados = $request->all();
        $dados['is_aval'] = isset($dados['is_aval']) ? 1 : 0;

        $colaborador = $this->colaborador->find($id);
        $colaborador->update($dados);
        return redirect()->route("colaboradores.index");
    }

    public function getRemove($id)
    {
        $itensCompetencias = $this->itensCompetencia->find($id);
        $itensCompetencias->delete();
        return redirect()->route("itens.competencias.index");
    }
}
