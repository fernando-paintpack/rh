<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Perfis;
use App\Usuarios;

use App\Http\Requests;

class UsuarioController extends Controller
{
    private $user;

    public function __construct(Usuarios $user)
    {
    	$this->user = $user;
    }

    public function login()
    {
        return view('login');
    }

    public function index()
    {
    	$users = Usuarios::all();
    	return view('usuarios.index', compact('users'));
    }

    public function getNova()
    {
        $allPerfis = Perfis::where('status', 1)->get();
        $perfis = [];
        foreach ($allPerfis as $perfil) {
            $perfis[$perfil->id] = $perfil->nome;
        }
        return view('usuarios.nova', compact('perfis'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome) || empty($request->sobrenome) || empty($request->email) || empty($request->senha) || empty($request->id_perfil)){
            return false;
        }

        header('Content-type: application/json');

        $rules = array(
            'email' => 'unique:usuarios,email'
        );

        $validator = Validator::make($request->only('email'), $rules);

        if ($validator->fails()) {
            echo json_encode([
                'error' => true,
                'message' => 'O email informado já está cadastrado no sistema.'
            ]);
            die;
        }

        $data = $request->all();

        $data['senha'] = bcrypt($data['senha']);

        $user = $this->user;
        $user->create($data);

        return redirect()->route('usuarios.index');
    }

    public function getEdit($id)
    {
        $user = $this->user->find($id);
        $allPerfis = Perfis::where('status', 1)->get();
        $perfis = [];
        foreach ($allPerfis as $perfil) {
            $perfis[$perfil->id] = $perfil->nome;
        }
        return view('usuarios.edit', compact('user', 'perfis'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome) || empty($request->sobrenome) || empty($request->id_perfil)){
            return false;
        }

        $user = $this->user->find($id);
        $user->update($request->all());

        return redirect()->route('usuarios.index');
    }

    public function getRemove($id)
    {
        $user = $this->user->find($id);
        $user->delete();

        return redirect()->route("usuarios.index");
    }

}
