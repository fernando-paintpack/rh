<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OsParametros;
use App\OsClientes;
use App\OsTiposCamposXml;

use App\Http\Requests;

class OsParametroController extends Controller
{
    private $parametro;

    public function __construct(OsParametros $parametro)
    {
    	$this->parametro = $parametro;
    }

    public function index()
    {
    	$parametros = OsParametros::all();
    	return view('os-parametros.index', compact('parametros'));
    }

    public function getNova()
    {
        $allClientes = OsClientes::all();
        $clientes = [];
        $allTiposCampos = OsTiposCamposXml::all();
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        foreach ($allTiposCampos as $tipoCampo) {
            $tiposCampos[$tipoCampo->id] = $tipoCampo->nome;
        }
        return view('os-parametros.nova', compact('clientes', 'tiposCampos'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome) && empty($request->sigla)){
            return false;
        }

        $parametro = $this->parametro;

        if(!is_null($request->input('clientes')))
            $parametro->create($request->all())->clientes()->sync($request->input('clientes'));
        else
            $parametro->create($request->all());

        return redirect()->route("os.parametros.index");
    }

    public function getEdit($id)
    {
        $clientes = [];
        $clientesIds = [];
        $editParametro = $this->parametro->find($id);
        $allClientes = OsClientes::all();
        $allTiposCampos = OsTiposCamposXml::all();
        foreach ($allClientes as $cliente) {
            $clientes[$cliente->id] = $cliente->nome;
        }
        foreach ($editParametro->clientes as $value) {
            $clientesIds[] = $value->id;
        }
        foreach ($allTiposCampos as $tipoCampo) {
            $tiposCampos[$tipoCampo->id] = $tipoCampo->nome;
        }
        return view('os-parametros.edit', compact('editParametro', 'clientes', 'clientesIds', 'tiposCampos'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome) && empty($request->sigla)){
            return false;
        }

        $parametro = $this->parametro->find($id);

        if(isset($request->clientes))
            $parametro->clientes()->sync($request->input('clientes'));
        else
            $parametro->clientes()->detach();

        $parametro->update($request->all());

        return redirect()->route("os.parametros.index");
    }

    public function getRemove($id)
    {
        $parametro = $this->parametro->find($id);
        $parametro->clientes()->detach();
        $parametro->delete();

        return redirect()->route("os.parametros.index");
    }
}
