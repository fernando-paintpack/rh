<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsTiposCamposXml extends Model
{
    protected $table = 'os_tipos_campos_xml';

    public function parametros()
    {
    	return $this->hasMany('App\OsParametros', 'id_tipo_campo');
    }
}
