<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsParametros extends Model
{
    protected $fillable = array(
        'id_tipo_campo',
        'nome',
        'sigla',
        'valor',
        'status'
    );

    public function clientes()
    {
    	return $this->belongsToMany('App\OsClientes', 'os_clientes_parametros', 'id_parametro', 'id_cliente');
    }

    public function tiposCampos()
    {
    	return $this->belongsTo('App\OsTiposCamposXml', 'id_tipo_campo');
    }

}
