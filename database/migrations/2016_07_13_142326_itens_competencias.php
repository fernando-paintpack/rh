<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItensCompetencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_competencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_competencia')->unsigned();
            $table->foreign('id_competencia')->references('id')->on('competencias');
            $table->string('descricao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('itens_competencias');
    }
}
