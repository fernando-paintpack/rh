<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OsCores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('os_cores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_servico')->unsigned();
            $table->foreign('id_servico')->references('id')->on('os_servicos');
            $table->string('nome');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('os_cores');
    }
}
