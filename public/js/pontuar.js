$(function () {

	$("form").validate({
		rules:{
			data_avaliacao:{
				required: true
			}
		},
		messages:{
			data_avaliacao:{
				required: "Preencha a Data"
			}
		}
	});

	$("#editData").mask("99/99/9999");

	$("#btn-pontuar").click( function() {
		$.ajax({
			url: window.location.origin + '/servicos/media-acao',
			data:new FormData($("form")[0]),
			type: "post",
			processData: false,
			contentType: false,
		}).done(function(data){
			$("#editExcelente").val(data.medias[1] + '%');
			$("#editBom").val(data.medias[2] + '%');
			$("#editRegular").val(data.medias[3] + '%');
			$("#editRuim").val(data.medias[4] + '%');
            $("#editNotaAcao").val(data.nota);
			$("#editObsAcao").val(data.acao);
			switch (data.nota) {
				case 'Excelente':
					$("#editNotaAcao").css("background-color", "#337AB7");
					break;
				case 'Bom':
					$("#editNotaAcao").css("background-color", "#5BC0DE");
					break;
				case 'Regular':
					$("#editNotaAcao").css("background-color", "#F0AD4E");
					break;
                case 'Ruim':
                    $("#editNotaAcao").css("background-color", "#D9534F");
                    break;
                default:
                    $("#editNotaAcao").css("background-color", "#EEEEEE");
			}
		});
	});

    $("#btn-pontuar").trigger("click");
});
