@extends('template')

@section('title')
	Itens de Competências
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Itens de Competências</h1>
        {{ Html::link('itens-competencias/nova', 'Novo Item de Competência', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($itensCompetencias) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Descrição</th>
    				<th>Competencia</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($itensCompetencias as $itemCompetencia)
    				<tr>
    					<td>{{ str_limit($itemCompetencia->descricao, 60) }}</td>
    					<td>{{ str_limit($itemCompetencia->competencias->descricao, 60) }}</td>
    					<td>
    						<a href="itens-competencias/edit/{{ $itemCompetencia->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="itens-competencias/remove/{{ $itemCompetencia->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse item de competência?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhuma Competência Localizada</h4>
    	</div>
    @endif

    </div>

@stop
