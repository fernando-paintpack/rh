@extends('template')

@section('title')
	Avaliações
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Editar Avaliação</h1>
    </div>

	<div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblColaborador', 'Colaborador', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('colaborador', $editAvaliacao->colaboradores->nome . ' ' . $editAvaliacao->colaboradores->sobrenome, array('class' => 'form-control', 'id' => 'editCargo', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('cargo', $editAvaliacao->colaboradores->cargo, array('class' => 'form-control', 'id' => 'editCargo', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('departamento', $editAvaliacao->colaboradores->depto, array('class' => 'form-control', 'id' => 'editDepartamento', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblAvaliador', 'Avaliador', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('id_avaliador', $avaliadores, $editAvaliacao->avaliadores->id, array('placeholder' => 'Selecione o Avaliador...', 'class' => 'form-control')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('avaliacoes', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
