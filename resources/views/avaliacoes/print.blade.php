<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PaintPack RH - Avaliação</title>

        <!-- Scripts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.8/js/jquery.tablesorter.js"></script>

        <!-- CSS -->
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/admin.css') }}" rel="stylesheet">

        <!-- Icone -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/common/icon.png') }}" />

        <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    </head>
    <body>

        <style>
            body{
                font-size: 11px;
            }
        </style>

        <div class="container">

            <div class="list-title-header" style="text-align: center;">
                <img src="{{ asset('/images/pp_logo.png') }}" alt="Logo" width="150" style="float: left; margin-top: 20px;">
            	<h1 style="width: 840px; float: left;">Avaliação de Desempenho</h1>
                <a href="javascript:void(0)" id="print" title="Imprimir">
                    <img src="{{ asset('/images/print.png') }}" alt="Logo" width="37" style="float: right; margin-top: 20px;" id="print-img">
                </a>
            </div>


            <div style="margin-top: 80px; ">
            	{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
            		<div class="form-group" style="clear: both; margin: 10px 0 !important;">
            			{!! Form::label('lblColaborador', 'Colaborador(a)', array('class' => 'control-label', 'style' => 'float: left; width: 200px; padding: 0;')) !!}
            			<div style="">
                            {{ $editAvaliacao->colaboradores->nome . ' ' . $editAvaliacao->colaboradores->sobrenome }}
            	    	</div>
            	    </div>
            		<div class="form-group" style="clear: both; margin: 10px 0 !important;">
            			{!! Form::label('lblCargo', 'Cargo', array('class' => 'control-label', 'style' => 'float: left; width: 200px; padding: 0;')) !!}
            			<div style="">
                            {{ $editAvaliacao->colaboradores->cargo }}
            	    	</div>
            	    </div>
            		<div class="form-group" style="clear: both; margin: 10px 0 !important;">
            			{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'control-label', 'style' => 'float: left; width: 200px; padding: 0;')) !!}
            			<div style="">
                            {{ $editAvaliacao->colaboradores->depto }}
            	    	</div>
            	    </div>
            		<div class="form-group" style="clear: both; margin: 10px 0 !important;">
            			{!! Form::label('lblAvaliador', 'Avaliador(a)', array('class' => 'control-label', 'style' => 'float: left; width: 200px; padding: 0;')) !!}
            			<div style="">
                            {{ $editAvaliacao->avaliadores->nome . ' ' . $editAvaliacao->avaliadores->sobrenome }}
            	    	</div>
            	    </div>
            		<div class="form-group" style="clear: both; margin: 10px 0 !important;">
            			{!! Form::label('lblData', 'Data da Avaliação', array('class' => 'control-label', 'style' => 'float: left; width: 200px; padding: 0;')) !!}
            			<div style="">
                            {{ $editAvaliacao->data_avaliacao }}
            	    	</div>
            	    </div>

                    <div style="margin: 30px 0; width: 100%; border: solid 1px #333333; padding: 20px 10px; line-height: 25px;">
                        <b>INSTRUÇÕES:</b> <br />
                        <p><small>
                            > Avalie os colaboradores individualmente em cada uma das competências, marcando um "X" em uma das cinco classificações. <br />
                            > Para competências não necessárias ao cargo avaliado, marcar a opção "Não Aplicável". <br />
                            > Converse com o colaborador sobre a sua avaliação, abordando os pontos fortes e também aqueles que precisam ser desenvolvidos.
                        </small></p>
                        <b>LEGENDAS:</b> <br />
                        <p><small>
                            > EXCELENTE: Supera as expectativas.<br />
                            > BOM: Dentro das expectativas.<br />
                            > REGULAR: Abaixo das expectativas com perspectiva de melhora.<br />
                            > RUIM: Abaixo das expectativas e sem perspectiva de melhora.<br />
                            > NÃO APLICÁVEL: Item não se aplica ao cargo avaliado.<br />
                        </small></p>
                    </div>

                    <h4 style="margin-bottom: 20px;">1. COMPETÊNCIAS TÉCNICAS E COMPORTAMENTAIS</h4>

            		@foreach($competencias as $competencia)
            			<table id="list-produtos" class="table table-striped">
            				<thead>
            					<tr>
            						<th>{{ $competencia->descricao }}</th>
            						@foreach($notas as $nota)
            							<th style="text-align: center;">{{ $nota->descricao }}</th>
            						@endforeach
            					</tr>
            				</thead>
            				<tbody>
            					@foreach($competencia->itensCompetencias as $itemCompetencia)
            						<tr>
            							<td style="width: 650px;">{{ $itemCompetencia->descricao }}</td>
            							@foreach($notas as $nota)
            								@if($nota->id == $itemCompetencia->nota)
            									<td style="text-align: center;">{{ Form::radio("notas[$itemCompetencia->id]", $nota->id, true) }}</td>
            								@else
            									<td style="text-align: center;">{{ Form::radio("notas[$itemCompetencia->id]", $nota->id, false, ['disabled']) }}</td>
            								@endif
            							@endforeach
            						</tr>
            					@endforeach
            				</tbody>
            			</table>
            		@endforeach

                    <h4 style="margin-top: 30px;">2. RESULTADO</h4>

            		<div class="form-group" style="clear: both; margin: 10px 0 !important;">
                        <div style="float: left; width: 25%; padding: 10px; text-align: center;">
                            {!! Form::label('lblExcelente', 'Excelente', array('class' => 'control-label', 'style' => 'float: left; padding: 0;')) !!}
                            <span id="editExcelente" style="border: solid 1px #BBBBBB; padding: 5px 25px;"></span>
            	    	</div>
            			<div style="float: left; width: 25%; padding: 10px; text-align: center;">
                            {!! Form::label('lblBom', 'Bom', array('class' => 'control-label', 'style' => 'float: left; padding: 0;')) !!}
            		    	<span id="editBom" style="border: solid 1px #BBBBBB; padding: 5px 25px;"></span>
            	    	</div>
            			<div style="float: left; width: 25%; padding: 10px; text-align: center;">
                            {!! Form::label('lblRegular', 'Regular', array('class' => 'control-label', 'style' => 'float: left; padding: 0;')) !!}
            		    	<span id="editRegular" style="border: solid 1px #BBBBBB; padding: 5px 25px;"></span>
            	    	</div>
            			<div style="float: left; width: 25%; padding: 10px; text-align: center;">
                            {!! Form::label('lblRuim', 'Ruim', array('class' => 'control-label', 'style' => 'float: left; padding: 0;')) !!}
            		    	<span id="editRuim" style="border: solid 1px #BBBBBB; padding: 5px 25px;"></span>
            	    	</div>
            	    </div>

                    <h4 style="margin-top: 30px;">3. AÇÃO RECOMENDADA</h4>

                    <small>
                        <b>3.1</b>&nbsp;
                        Somente Avaliação de Desempenho <br />
                        <b>Excelente</b><br />
                        <b>Bom</b>
                        <br /><br />
                        <b>3.2</b>&nbsp;
                        Colocar o Colaborador em observação e rever novamente em: <br />
                        <b>Regular</b> <br />
                        <b>Ruim</b>
                    </small>

                    <div class="form-group" style="clear: both; margin: 20px 0 !important;">
                        <div style="float: left; width: 30%; padding: 10px; text-align: center;">
                            {!! Form::label('lblResultado', 'Resultado', array('class' => 'control-label', 'style' => 'float: left; padding: 0;')) !!}
                            <span style="border: solid 1px #BBBBBB; padding: 5px 25px;">{{ $editAvaliacao->nota_acao }}</span>
            	    	</div>
            			<div style="float: left; width: 7%; padding: 10px; text-align: center;">
                            {!! Form::label('lblObs', 'Ação', array('class' => 'control-label', 'style' => 'float: left; padding: 0;')) !!}
            	    	</div>
                        <div style="float: left; border: solid 1px #BBBBBB; padding: 5px; width: 58%; margin-top: 6px;">{{ $editAvaliacao->obs_acao }}</div>
                    </div>

                    <h4 style="margin-top: 30px;">4. OBSERVAÇÕES</h4>

            		<div class="form-group" style="clear: both; margin: 20px 0 70px;">
            			{!! Form::label('lblObs', 'Comentários Gerais', array('class' => 'col-sm-1 control-label')) !!}
            			<div class="col-sm-11">
            		    	{!! Form::textarea('obs_acao', $editAvaliacao->obs_finais, array('class' => 'form-control', 'id' => 'editObsAcao', 'disabled')) !!}
            	    	</div>
            	    </div>

            	{!! Form::close() !!}
            </div>

            <div style="float: left; width: 33%; text-align: center;">
                __________________________________
                <br />
                <small>Assinatura do Colaborador(a)</small>
            </div>

            <div style="float: left; width: 33%; text-align: center;">
                __________________________________
                <br />
                <small>Assinatura do Gestor(a)</small>
            </div>

            <div style="float: left; width: 33%; text-align: center;">
                __________________________________
                <br />
                <small>Assinatura do Diretor Geral</small>
            </div>

        </div>

        <script>
            $(function () {
        		$.ajax({
        			url: window.location.origin + '/servicos/media-acao',
        			data:new FormData($("form")[0]),
        			type: "post",
        			processData: false,
        			contentType: false,
        		}).done(function(data){
        			$("#editExcelente").text(data.medias[1] + '%');
        			$("#editBom").text(data.medias[2] + '%');
        			$("#editRegular").text(data.medias[3] + '%');
        			$("#editRuim").text(data.medias[4] + '%');
        		});

                $("#print").click( function() {
                    $("#print-img").hide();
                    $("h1").css("width", "500px");
                    window.print();
                    $("#print-img").show();
                    $("h1").css("width", "840px");
                });
            });
        </script>

    </body>
</html>
