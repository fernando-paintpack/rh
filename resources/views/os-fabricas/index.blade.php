@extends('template')

@section('title')
	OS - Fábricas
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Fábricas</h1>
        {{ Html::link('os/fabricas/novo', 'Nova Fábrica', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($fabricas) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
    				<th>Sigla</th>
					<th>Status</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($fabricas as $fabrica)
    				<tr>
    					<td>{{ $fabrica->nome }}</td>
						<td>{{ $fabrica->sigla }}</td>
    					<td>{{ $fabrica->status ? 'Ativo' : 'Inativo' }}</td>
    					<td>
    						<a href="fabricas/edit/{{ $fabrica->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="fabricas/remove/{{ $fabrica->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover essa fábrica?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhuma Fábrica Localizada</h4>
    	</div>
    @endif

    </div>

@stop
