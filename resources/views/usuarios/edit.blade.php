@extends('template')

@section('title')
	Usuário
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Editar Usuário</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', $user->id, array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', $user->nome, array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSobrenome', 'Sobrenome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('sobrenome', $user->sobrenome, array('class' => 'form-control', 'id' => 'editSobrenome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblEmail', 'Email', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('email', $user->email, array('class' => 'form-control', 'id' => 'editEmail')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('cargo', $user->cargo, array('class' => 'form-control', 'id' => 'editCargo')) !!}
				</div>
			</div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('depto', $user->depto, array('class' => 'form-control', 'id' => 'editDepartamento')) !!}
				</div>
			</div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblPerfil', 'Perfil', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('id_perfil', $perfis, $user->id_perfil, array('placeholder' => 'Selecione a Perfil...', 'class' => 'form-control', 'id' => 'editPerfil')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), $user->status, array('class' => 'form-control', 'id' => 'editStatus')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('usuarios', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
