@extends('template')

@section('title')
	Usuário
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Novo Usuário</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSobrenome', 'Sobrenome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('sobrenome', '', array('class' => 'form-control', 'id' => 'editSobrenome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblEmail', 'Email', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('email', '', array('class' => 'form-control', 'id' => 'editEmail')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSenha', 'Senha', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::password('senha', array('class' => 'form-control', 'id' => 'editSenha')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('cargo', '', array('class' => 'form-control', 'id' => 'editCargo')) !!}
				</div>
			</div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('depto', '', array('class' => 'form-control', 'id' => 'editDepartamento')) !!}
				</div>
			</div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblPerfil', 'Perfil', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('id_perfil', $perfis, null, array('placeholder' => 'Selecione a Perfil...', 'class' => 'form-control', 'id' => 'editPerfil')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('usuarios', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
