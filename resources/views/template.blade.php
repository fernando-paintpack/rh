<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PaintPack - @yield('title')</title>

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.8/js/jquery.tablesorter.js"></script>

    <!-- CSS -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/admin.css') }}" rel="stylesheet">

    <!-- Icone -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/common/icon.png') }}" />

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <img src="{{ asset('/images/pp_logo.png') }}" alt="Logo" width="100">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if (Auth::user()->id_perfil == 1)
                    <li>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Usuários <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{{ link_to_route('usuarios.index', 'Lista de Usuários') }}</li>
                                    <li>{{ link_to_route('perfis.index', 'Perfis') }}</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (in_array(Auth::user()->id_perfil, [1, 2, 3]))
                    <li>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Avaliações <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{{ link_to_route('avaliacoes.index', 'Lista de Avaliações') }}</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (in_array(Auth::user()->id_perfil, [1, 2]))
                    <li>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Colaboradores <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{{ link_to_route('colaboradores.index', 'Lista de Colaboradores') }}</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Competências <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{{ link_to_route('competencias.index', 'Lista de Competências') }}</li>
                                    <li>{{ link_to_route('itens.competencias.index', 'Itens de Competências') }}</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (in_array(Auth::user()->id_perfil, [1, 4, 5]))
                    <li>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Ordens de Serviço <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{{ link_to_route('os.servicos.index', 'Lista de OS') }}</li>
                                    @if (in_array(Auth::user()->id_perfil, [1, 4]))
                                        <li>{{ link_to_route('os.clientes.index', 'Clientes') }}</li>
                                        <li>{{ link_to_route('os.fabricas.index', 'Fábricas') }}</li>
                                        <li>{{ link_to_route('os.volumes.index', 'Volumes') }}</li>
                                        <li>{{ link_to_route('os.saidas.index', 'Tipos de Saídas') }}</li>
                                        <li>{{ link_to_route('os.parametros.index', 'Parâmetros de OS') }}</li>
                                        <li>{{ link_to_route('os.servicos.get.destinatarios', 'Destinatários de Email de OS') }}</li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endif

            </ul>
            @if (!Auth::guest())
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Olá {{ Auth::user()->nome }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
    </div>
</nav>

<div class="container">
    @yield('content')
</div>

</body>
</html>
