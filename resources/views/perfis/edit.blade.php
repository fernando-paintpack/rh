@extends('template')

@section('title')
	Perfis
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Editar Perfil</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', $editPerfil->nome, array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblAvaliador', 'Avaliador', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), $editPerfil->status ? 'Ativo' : 'Inativo', array('class' => 'form-control', 'id' => 'editStatus')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('perfis', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
