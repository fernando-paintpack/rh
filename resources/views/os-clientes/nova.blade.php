@extends('template')

@section('title')
	OS - Clientes
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Novo Cliente</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package', 'files' => 'true')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSigla', 'Sigla', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('sigla', '', array('class' => 'form-control', 'id' => 'editSigla')) !!}
		    	</div>
		    </div>
			<div class="form-group" id="logo">
				{!! Form::label('lblLogo', 'Logotipo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-1">
					{{ Html::image(asset('/images/noImage.png'), 'alt', array('class' => 'img-rounded img-logo')) }}
				</div>
			</div>
			<div class="form-group" id="logo">
				{!! Form::label('lblLogo', ' ', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2" style="height: 40px;">
					{!! Form::file('logo') !!}
				</div>
			</div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('os/clientes', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
