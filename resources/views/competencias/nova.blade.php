@extends('template')

@section('title')
	Competências
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Nova Competência</h1>
    </div>

    <div class="">

		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDescricao', 'Descrição', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::textarea('descricao', '', array('class' => 'form-control', 'id' => 'editDescricao')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblOrdem', 'Ordem', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('ordem', $ordem, null, array('placeholder' => 'Selecione a Posição...', 'class' => 'form-control')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('competencias', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}

	</div>

@stop
