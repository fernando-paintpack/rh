<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PaintPack - Imprimir OS</title>

    <!--css-->
	<link href="{{ asset('/css/print.css') }}" rel="stylesheet">

    <!-- Icone -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/common/icon.png') }}" />

</head>
<body>
    <script>
            window.print();
	</script>
    <div id="conteudo">
        <div>
            <div class="imgL">
                <img src="{{ asset('/images/pp_print.png') }}" alt="Logo">
            </div>
            <div class="imgR">
                <img src="{{ asset($cliente->path_logo ? $cliente->path_logo : '/images/noImage.png') }}" alt="Logo">
            </div>
        </div>
        <div class="title">
            <h1>ORDEM DE PRODUÇÃO DRYOFFSET</h1>
        </div>
        <div class="bloco">
            <p>NÚMERO JOB: <span class="valor">{{ $servico->job }}</span></p>
            <p>RÓTULO: <span class="valor">{{ $servico->rotulo }}</span></p>
            <p>VOLUME: <span class="valor">{{ $parametros['VOLUME'] }}</span></p>
            <p>TIPO DE SAÍDA: <span class="valor">{{ $parametros['SAIDA'] }}</span></p>
            <p>DATA DE ENTRADA: <span class="valor">{{ $servico->dt_entrada }}</span></p>
            <p>JOB ANTERIOR: <span class="valor">{{ $servico->job_anterior }}</span></p>
            @if(!empty($servico->codigo_exal))
                <p>CÓDIGO EXAL: <span class="valor">{{ $servico->codigo_exal }}</span></p>
                <p>
                    CÓDIGO DO CLIENTE: <span class="valor">{{ $servico->codigo }}</span>
                    &emsp;PRODUÇÃO DE CARTELA: <span class="valor">{{ $parametros['CARTELA'] ? 'SIM' : 'NÃO' }}</span>
                    @if(!empty($parametros['PLACAS-DIGITAIS']) && $parametros['SAIDA'] == 'CDI')
                        &emsp;QTDE PLACAS: <span class="valor">{{ $parametros['PLACAS-DIGITAIS'] }}</span>
                    @endif
                </p>
            @else
                <p>
                    CÓDIGO DE DESENVOLVIMENTO: <span class="valor">{{ $servico->codigo }}</span>
                    &emsp;PRODUÇÃO DE CARTELA: <span class="valor">{{ $parametros['CARTELA'] ? 'SIM' : 'NÃO' }}</span>
                    @if(!empty($parametros['PLACAS-DIGITAIS']) && $parametros['SAIDA'] == 'CDI')
                        &emsp;QTDE PLACAS: <span class="valor">{{ $parametros['PLACAS-DIGITAIS'] }}</span>
                    @endif
                </p>
            @endif
        </div>

        <div class="bloco">
            <p>CÓDIGO DE BARRAS: <span class="valor">{{ $parametros['BARCODE'] }}</span></p>
            <p>COR BARRAS: <span class="valor">{{ $parametros['COR-BARRAS'] }}</span>
            &emsp;COR NÚMEROS: <span class="valor">{{ $parametros['COR-BARRAS-NUMEROS'] }}</span></p>
            <p>COR TEXTO LEGAL: <span class="valor">{{ $parametros['COR-TEXTO-LEGAL'] }}</span>
            &emsp;COR TAB. NUTRICIONAL: <span class="valor">{{ $parametros['COR-TAB-NUTRICIONAL'] }}</span></p>
            <p>TEXTO EDITÁVEL: <span class="valor">{{ $parametros['TEXTO-EDITAVEL'] ? 'SIM' : 'NÃO' }}</span>
            &emsp;TIPO TEXTO: <span class="valor">{{ $parametros['TIPO-TEXTO'] ? 'POSITIVO' : 'NEGATIVO' }}</span></p>
        </div>

        <div class="bloco">
            <p>CORES:</p>
            @for ($i = 1; $i <= 8; $i++)
                <p class="cores">
                    {{ $i }}: <span class="valor">{{ isset($cores[$i-1]) ? $cores[$i-1] : '' }}</span>
                </p>
                @if($i == 4)
                    </p>
                    <p>
                @endif
            @endfor
        </div>

        <div class="bloco">
            <p>FÁBRICAS:</p>
            <p>
                <span class="valor">{{ $fabricas ? $fabricas : '' }}</span>
            </p>
        </div>

        <div class="bloco">
            <p>PRODUZIDA AMOSTRA?: <span class="valor">{{ $parametros['TEVE-AMOSTRA'] ? 'SIM' : 'NÃO' }}</span></p>
            <p>
                CÓDIGO: <span class="valor">{{ $parametros['COD-AMOSTRA'] }}</span>&emsp;
                VERSÃO: <span class="valor">{{ $parametros['VERSAO-AMOSTRA'] }}</span>
            </p>

        </div>

        <div class="bloco">
            <p>OBSERVAÇÕES:</p>
            <p>
                <span class="valor">{{ $servico->observacoes }}</span>
            </p>
            <p>
                <span class="valor">{{ $parametros['CONTATO1'] }}</span>
            </p>
            <p>
                <span class="valor">{{ $parametros['CONTATO2'] }}</span>
            </p>
        </div>
    </div>
</body>
</html>
