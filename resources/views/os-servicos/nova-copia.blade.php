@extends('template')

@section('title')
	OS - Serviços
@stop

@section('content')

	<script src="{{ asset('/js/os-servicos.js') }}"></script>

    <div class="list-title-header">
    	<h1>Editar Serviço</h1>
    </div>
	<div class="messages">
		@if (session('erro'))
			<div class="alert-success">
				{{ session('erro') }}
			</div>
		@endif
	</div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', $servico->id, array('id' => 'editId')) }}
			<div class="form-group">
				{!! Form::label('lblCliente', 'Cliente', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::select('id_cliente', $clientes, $servico->id_cliente, array('class' => 'form-control', 'id' => 'editClientes')) !!}
		    	</div>
				{!! Form::label('lblSaida', 'Saída', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::select('tipo_saida', $saidas, $servico->tipo_saida, array('placeholder' => 'Selecione o Tipo de Saída...', 'class' => 'form-control', 'id' => 'editTipoSaida')) !!}
		    	</div>
				{!! Form::label('lblVolume', 'Volume', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::select('volume', $volumes, strtolower($xml['VOLUME']), array('class' => 'form-control', 'id' => 'editClientes')) !!}
		    	</div>
		    </div>

			<div class="form-group">
				{!! Form::label('lblRotulo', 'Rótulo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
			    	{!! Form::text('rotulo', $servico->rotulo, array('class' => 'form-control', 'id' => 'editSigla')) !!}
		    	</div>
				{!! Form::label('lblJob', 'Num. Job', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
					{!! Form::text('job_num', '', array('class' => 'form-control', 'id' => 'editJobNum')) !!}
				</div>
				<div class="col-sm-1">
					{!! Form::text('job_cli', $servico->job_cli, array('class' => 'form-control', 'id' => 'editJobCli', 'readonly')) !!}
				</div>
				{!! Form::label('lblRev', 'Revisão', array('class' => 'col-sm-1 control-label', )) !!}
				<div class="col-sm-1">
					{!! Form::text('job_rev', '', array('class' => 'form-control', 'id' => 'editRevisao')) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lblEntrada', 'Data', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
					{!! Form::text('dt_entrada', $servico->dt_entrada, array('class' => 'form-control', 'id' => 'editEntrada', 'placeholder' => 'dd/mm/aaaa')) !!}
				</div>
				@if($cliente->nome == 'Exal')
					<div id="campo-exal">
						{!! Form::label('lblCodExal', 'Cód. Exal', array('class' => 'col-sm-1 control-label', 'style' => 'width: 8%;')) !!}
						<div class="col-sm-2">
							{!! Form::text('codigo_exal', $servico->codigo_exal, array('class' => 'form-control', 'id' => 'editCodExal')) !!}
						</div>
					</div>
				@endif
				{!! Form::label('lblCodigo', 'Cód. Cliente', array('class' => 'col-sm-1 control-label', 'style' => 'width: 8%;')) !!}
				<div class="col-sm-2">
					{!! Form::text('codigo', $servico->codigo, array('class' => 'form-control', 'id' => 'editCodigo')) !!}
				</div>
				{!! Form::label('lblJobAnterior', 'Job Anterior', array('class' => 'col-sm-1 control-label', 'style' => 'width: 9%;')) !!}
				<div class="col-sm-2">
					{!! Form::text('job_anterior', $servico->job_anterior, array('class' => 'form-control', 'id' => 'editJobAnterior', 'placeholder' => '00-1234-XX-R1')) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lblEntrada', 'Contato 1', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('contato_um', isset($xml['CONTATO1']) ? strtolower($xml['CONTATO1']) : '', array('class' => 'form-control', 'id' => 'editContato1', 'placeholder' => 'contato@email.com')) !!}
				</div>
				{!! Form::label('lblCodigo', 'Contato 2', array('class' => 'col-sm-1 control-label', 'style' => 'width: 8%;')) !!}
				<div class="col-sm-5">
					{!! Form::text('contato_dois', isset($xml['CONTATO2']) ? strtolower($xml['CONTATO2']) : '', array('class' => 'form-control', 'id' => 'editContato2', 'placeholder' => 'contato@email.com')) !!}
				</div>
			</div>

			<div class="form-group" id="listFabricas">
				{!! Form::label('lblFabricas', 'Fábricas', array('class' => 'col-sm-1 control-label')) !!}
				<div id="lista-fabricas">
					<?php $i = 1; ?>
					@foreach($fabricas as $key => $fabrica)
						@if(in_array($key, $servico->clienteFabricas))
							<div class="col-sm-2 control-label" style="text-align: left">
								{{ Form::checkbox('fabricas[]', $fabrica['sigla'], in_array($fabrica['sigla'], $servico->fabricas) ? true : false) }}&nbsp;
								{{ $fabrica['nome'] }}
							</div>
							@if($i % 5 == 0)
								<br /><br />
								{!! Form::label('lblCores', ' ', array('class' => 'col-sm-1 control-label')) !!}
							@endif
							<?php $i++; ?>
						@endif
					@endforeach
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lblCliente', 'Observações', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::textarea('observacoes', $servico->observacoes, array('class' => 'form-control', 'id' => 'editObs')) !!}
		    	</div>
		    </div>

			<hr>

			<div class="form-group" id="box-cores">
				{!! Form::label('lblCores', 'Cores', array('class' => 'col-sm-1 control-label')) !!}
				@for ($i = 1; $i < 9; $i++)
					<div class="col-sm-2" style="width: 22%;">
						{!! Form::text('cores[]', isset($servico->cores[$i-1]) ? $servico->cores[$i-1]['nome'] : '', array('class' => 'form-control', 'id' => 'editCores')) !!}
					</div>
					@if($i % 4 == 0)
						<br /><br />
						{!! Form::label('lblCores', ' ', array('class' => 'col-sm-1 control-label')) !!}
					@endif
				@endfor
				{!! Form::label('lblCores', ' ', array('class' => 'col-sm-1 control-label')) !!}
				{!! Form::button('Carregar Cores', array('class' => 'btn btn-warning', 'style' => 'float: right; margin-right: 58px;', 'id' => 'btn-cores')) !!}
			</div>

			<hr>

			<div class="form-group" id="listParametros">
				<div id="lista-parametros">
					<?php $i == 1; ?>
					@foreach($cliente->parametros as $parametro)
						@if(isset($xml[$parametro->sigla]))
							@if($parametro->id_tipo_campo != 3)
								{!! Form::label('lblParametros', $parametro->nome, array('class' => 'col-sm-2 control-label')) !!}
							@endif
							<div class="col-sm-4">
								@if(is_integer(strpos($parametro->sigla, 'COR')))
									{!! Form::select("parametros[$parametro->sigla]", [ucwords(strtolower($xml[$parametro->sigla])) => ucwords(strtolower($xml[$parametro->sigla]))], null, array('class' => 'form-control selectCores', 'readonly')) !!}
								@elseif($parametro->id_tipo_campo == 1)
									{!! Form::text("parametros[$parametro->sigla]", strtoupper($xml[$parametro->sigla]), array('class' => 'form-control')) !!}
								@elseif($parametro->id_tipo_campo == 3)
									{{ Form::hidden("parametros[$parametro->sigla]", ucwords(strtolower($xml[$parametro->sigla])), array('id' => 'editValor')) }}
								@else
									{{ Form::radio("parametros[$parametro->sigla]", 1, $xml[$parametro->sigla] ? true : false, ['class' => 'field']) }} Sim
									&emsp;
									{{ Form::radio("parametros[$parametro->sigla]", 0, $xml[$parametro->sigla] ? false : true, ['class' => 'field']) }} Não
								@endif
							</div>
							@if($i % 2 == 0)
								<br /><br /><br />
							@endif
							<?php $i++; ?>
						@endif
					@endforeach
				</div>
			</div>

    		<div class="modalButtons">
				{{ Html::link('os/servicos', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

	<script>
		$(function () {
			$("#editJobAnterior").mask("99-9999 " + $('#editJobCli').val() + "-R9");
		});
	</script>

@stop
