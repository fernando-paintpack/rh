@extends('template')

@section('title')
	OS - Destinatários de Email
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Destinatários de Email</h1>
    </div>

    <div class="table-responsive clear">

        @if (count($colaboradores) >= 1)
            {!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
            	<table id="list-package" class="table table-striped">
                    <thead>
            			<tr>
                            <th colspan="3">Selecione os colaboradores que receberão os emails com as OS's criadas</th>
            			</tr>
            		</thead>
            		<tbody>
                        <?php $count = 1; ?>
                        @foreach($colaboradores as $colaborador)
                            <td>
                                {{ Form::checkbox('destinatarios[]', $colaborador->id, $colaborador->dest_email_os) }}&ensp;
                                {{ $colaborador->nome . ' ' . $colaborador->sobrenome }}
                            </td>
                            @if($count % 3 == 0)
                                </tr>
                                <tr>
                            @endif
                            <?php $count++; ?>
                        @endforeach
            		</tbody>
            	</table>
                <div class="modalButtons">
    			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
    			</div>

    		{!! Form::close() !!}
        @else
        	<div>
        		<h4>Nenhum Colaborador Localizado</h4>
        	</div>
        @endif

    </div>

@stop
