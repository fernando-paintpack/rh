<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,700" rel="stylesheet">
		<style type="text/css">
		  body {
		    background-color: #98AFC7;
		    color: #444;
		    font-family: 'Open Sans', sans-serif;
		    font-size: 100%/30px;
		    text-shadow: 0 1px 0 #fff;
		  }
		  strong {
		  	font-weight: bold;
		  }
		  table {
		  	background-color: white;
		  }
		  th {
		  	background-color: #777;
		  	border-left: 1px solid #555;
		  	border-right: 1px solid #777;
		  	border-top: 1px solid #555;
		  	border-bottom: 1px solid #333;
		  	box-shadow: inset 0 1px 0 #999;
		  	color: #fff;
		    font-weight: bold;
		  	padding: 10px 15px;
		  	position: relative;
		  	text-shadow: 0 1px 0 #000;
		  }
		  td {
		  	border-right: 1px solid #fff;
		  	border-left: 1px solid #e8e8e8;
		  	border-top: 1px solid #fff;
		  	border-bottom: 1px solid #e8e8e8;
		  	padding: 10px 15px;
		  	position: relative;
		  }
		  td.paran{
		  	color: gray;
		  	font-weight: 300;
		  	width: 35%;
		  }
		  td.valor{
		  	color: red;
		  	font-weight: 700;
		  }
		  td.rodape {
		  	background-color: #e6e6e6;
		  	color: #666;
		  	font-size: 15px;
		  	font-weight: 300;
		  }
		</style>
	</head>
	<body>
		<table width="650" cellspacing="0" cellpadding="30" border="1" style="margin: 0 auto; text-align: left;">
			<tr>
				<th colspan="2"><strong>{{ $job }}</strong></th>
		    </tr>
		    <tr>
		    	<td class="paran">CLIENTE:</td> <td class="valor">{{ $parametros['CLIENTE'] }}</td>
		    </tr>
		    <tr>
		    	<td class="paran">ORDEM DE SERVIÇO:</td> <td class="valor">{{ $parametros['JOB'] }}</td>
		    </tr>
		    <tr>
		    	<td class="paran">RÓTULO:</td> <td class="valor">{{ $parametros['JOBNAME'] }}</td>
		    </tr>
		@if(isset($parametros['CODDESENVOLVIMENTO']))
		    <tr>
		    	<td class="paran">CÓD. DESENVOLVIMENTO:</td> <td class="valor">{{ $parametros['CODDESENVOLVIMENTO'] }}</td>
		    </tr>
		@endif
		@if(isset($parametros['CODEXAL']))
		    <tr>
		    	<td class="paran">CÓD. EXAL:</td> <td class="valor">{{ $parametros['CODEXAL'] }}</td>
		    </tr>
		    <tr>
		    	<td class="paran">CÓD. CLIENTE:</td> <td class="valor">{{ $parametros['CODCLIENTE'] }}</td>
		    </tr>
		@endif
			<tr>
				<td class="paran">DATA:</td> <td class="valor">{{ $parametros['DATA_OS'] }}</td>
			</tr>
			<tr>
				<td class="paran">OBS:</td> <td class="valor">{{ $observacoes }}</td>
			</tr>
			<tr>
				<td class="paran">CORES:</td>
				<td class="valor">
					<ol>
						@if(!empty($cores))
						    @foreach($cores as $cor)
							<li>{{ strtoupper($cor) }}</li>
							@endforeach
						@endif
					</ol>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="rodape">
		            EM ANEXO XML COM OS PARÂMETROS DESSE TRABALHO.
		            <br />
		            CONFIRA ATENTAMENTE OS DADOS CARREGADOS ANTES DE PROSSEGUIR COM O DESENVOLVIMENTO.
				</td>
			</tr>
		</table>
	</body>
</html>
