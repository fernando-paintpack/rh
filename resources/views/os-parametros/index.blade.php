@extends('template')

@section('title')
	OS - Parâmetros
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Parâmetros</h1>
        {{ Html::link('os/parametros/novo', 'Novo Parâmetro de OS', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($parametros) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
					<th>Sigla</th>
    				<th>Tipo</th>
					<th>Status</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($parametros as $parametro)
    				<tr style="height: 47px;">
    					<td>{{ $parametro->nome }}</td>
						<td>{{ $parametro->sigla }}</td>
						<td>{{ $parametro->id_tipo_campo == 1 ? 'Texto' : ($parametro->id_tipo_campo == 2 ? 'Booleano' : 'Hidden') }}</td>
    					<td>{{ $parametro->status ? 'Ativo' : 'Inativo' }}</td>
    					<td>
						@if(!in_array($parametro->id, range(1,11)))
    						<a href="parametros/edit/{{ $parametro->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="parametros/remove/{{ $parametro->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse parâmetro?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
						@endif
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhuma Parâmetro Localizado</h4>
    	</div>
    @endif

    </div>

@stop
