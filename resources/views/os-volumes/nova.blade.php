@extends('template')

@section('title')
	OS - Volumes
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Volumes</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('os/volumes', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
